#!/bin/bash

docker stop influx
docker rm influx
docker run -d --network host --name=influx -p 8086:8086 -v /opt/collector/bridges-influxdb/influxdb_data:/var/lib/influxdb -e INFLUXDB_ADMIN_USER=admin -e INFLUXDB_ADMIN_PASSWORD=bridges influxdb:1.8-alpine
