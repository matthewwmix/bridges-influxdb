# Description
The influxdb time series database that is configured to receive the pipeline data.

## Helpful links
https://docs.influxdata.com/influxdb/v1.7/introduction/getting-started/
https://docs.influxdata.com/influxdb/v1.8/query_language/explore-schema/

# Using
```bash
./start.sh

docker exec -it influx /bin/bash
influx
```
